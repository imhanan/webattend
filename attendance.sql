-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2016 at 08:23 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `attendance`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(44) NOT NULL,
  `password` varchar(44) NOT NULL,
  `roll_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lan_id` int(11) NOT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`lid`, `username`, `password`, `roll_id`, `user_id`, `lan_id`) VALUES
(1, 'han', '83832391027a1f2f4d46ef882ff3a47d', 0, 0, 0),
(2, 'bini', 'c1111bd512b29e821b120b86446026b8', 2, 6, 1),
(3, 'at', '4687a3f12b814d5c5b87e5268f2195e3', 1, 7, 3),
(5, 'akh', '8cab4b3e958f712569f51a9b90c495a7', 3, 9, 1),
(10, 'bin', '122071b16111f065b433d3efa8e69a2a', 1, 14, 1),
(11, 'rej', '17051f72ad116166f0de9841c4db7998', 1, 15, 1),
(12, 'hanan', '83832391027a1f2f4d46ef882ff3a47d', 1, 16, 1),
(13, 'allf', '893f53c159eab9178ab181bad8da4262', 1, 17, 1),
(14, 'at', '7d0db380a5b95a8ba1da0bca241abda1', 1, 17, 1),
(15, 'jam', '5275cb415e5bc3948e8f2cd492859f26', 1, 17, 1),
(16, 'han', '83832391027a1f2f4d46ef882ff3a47d', 1, 20, 1),
(17, 'gan', 'f1253bc7b6c0b1d62eb9b97cfebf0f63', 1, 17, 1),
(18, 'mad', '7538ebc37ad0917853e044b9b42bd8a4', 1, 17, 1),
(19, 'bini', 'c1111bd512b29e821b120b86446026b8', 1, 17, 1),
(20, 'use', '5ef76d30bf9232902687324b5bfa0bd2', 1, 17, 1),
(21, 'bin', 'c1111bd512b29e821b120b86446026b8', 1, 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(44) NOT NULL,
  `project` varchar(44) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `language`, `project`) VALUES
(1, '', 'Webqua Attendance'),
(2, 'Android', 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `roll`
--

CREATE TABLE IF NOT EXISTS `roll` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `roll` varchar(44) NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roll`
--

INSERT INTO `roll` (`rid`, `roll`) VALUES
(1, 'User'),
(2, 'Admin'),
(3, 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(44) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `section`) VALUES
(1, 'PHP'),
(2, 'Android'),
(3, 'Design');

-- --------------------------------------------------------

--
-- Table structure for table `timesheet`
--

CREATE TABLE IF NOT EXISTS `timesheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `Date` date NOT NULL,
  `fromdate` varchar(44) NOT NULL,
  `todate` varchar(44) NOT NULL,
  `description` text NOT NULL,
  `approved` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `timesheet`
--

INSERT INTO `timesheet` (`id`, `user_id`, `project_id`, `Date`, `fromdate`, `todate`, `description`, `approved`) VALUES
(11, 6, 1, '2016-02-10', '11:55:15 AM', '11:55:15 AM', 'hjhjhk', 0),
(12, 6, 1, '2016-02-10', '12:00:30 PM', '12:02:15 PM', 'ghjghgh', 0),
(13, 6, 1, '2016-02-12', '12:13:00 PM', '12:12:00 PM', 'commmpletee', 0),
(14, 7, 1, '2016-02-12', '12:51:30 PM', '2:51:30 PM', 'ghghgjhg', 0),
(15, 7, 1, '2016-02-12', '2:40:45 PM', '3:40:45 PM', 'gonee', 0),
(18, 2, 1, '0000-00-00', '8:04:30 AM', '8:04:30 PM', 'completed', 0),
(30, 9, 1, '2016-02-13', '12:46:00 PM', '12:46:00 PM', 'completeddafafaf', 2),
(31, 9, 1, '2016-02-15', '2:30 AM', '1:22 PM', 'mnmn,mm', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(44) NOT NULL,
  `email` varchar(44) NOT NULL,
  `number` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `number`) VALUES
(1, 'hak', 'hak@gm.com', '8989898789'),
(2, 'bin', 'bin@gm.com', '6787876787'),
(3, 'akhill', 'ak@gm.com', '6786787898'),
(4, 'ak', 'ah@gm.com', '7878787877'),
(5, 'hel', 'hel@gm.com', '7898989899'),
(9, 'Akhil krishna', 'akh@gm.com', '0'),
(14, 'bindya', 'bi@gm.com', '(678) 787-8989'),
(15, 'rejeena', 'rej@gm.com', '(456) 543-2123'),
(16, 'hanan', 'hanankabir@gmail.com', '(828) 104-9774'),
(17, 'alfy', 'bindyabinzz@gmail.com', '(787) 678-7898'),
(18, 'athul', 'bindyabinzz@gmail.com', '(676) 565-4567'),
(19, 'jam', 'bindyabinzz@gmail.com', '(657) 898-7656'),
(20, 'hghg', 'bindyabiinzz@gmail.com', '(876) 787-6788'),
(21, 'binnn', 'bindyabinzz@gmail.com', '(676) 777-7777'),
(22, 'madyy', 'bindyabinzz@gmail.com', '(123) 456-7777'),
(23, 'hhjgh', 'bindyabinzz@gmail.com', '(678) 767-8888'),
(24, 'houoiu', 'bindyabinzz@gmail.com', '(676) 567-7777'),
(25, 'hjgjhg', 'bindyabinzz@gmail.com', '(987) 987-8789');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
