
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	/*To call Login Screen function*/
	public function index()
	{
		$this->login();
		


	}
	public function home()
	{
		
		$this->load->view("admin_1/starter");


	}
	/*To View Login Screen*/
	public function login()
	{
		$this->load->view('admin_1/page_user_login_1');
	}

	/*Validates the login functionality*/
	public function login_validation()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username','Username','required|callback_validate_credentials');
		$this->form_validation->set_rules('password','Password','required');	

		if($this->form_validation->run())
		{
			
			$ids=$this->model_users->can_login();
			$i=array(
				'id'=>$ids->user_id,
				'username'=>$ids->username,
				);
			$sess=$this->session->set_userdata($i);
			$rid=$this->model_users->previlage();
			$ri=array(
				'rid'=>$rid->rid,
				'lan'=>$rid->lan_id,
				);
			$session=$this->session->set_userdata($ri);

			$this->load->view("admin_1/starter");

		}	
		else 

		{
			$data['message'] = ' Incorrect password or username';
			$this->load->view("admin_1/page_user_login_1",$data);
		}					
	}

	/*Checks credentiality of login users*/
	public function validate_credentials()
	{
		$this->load->model('model_users');
		if($this->model_users->can_login())
		{
			$ids=$this->model_users->can_login();
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_credentials','Incorrect Username/Password');
			return false;
		}
	}

	/*To register user details*/
	public function reg_validation()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('email','email','required');	
		$this->form_validation->set_rules('number','number','required');
		$this->form_validation->set_rules('role','role','required');
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');	
		

		if($this->form_validation->run())
		{
			
		$this->load->model('model_users');
		$name= $this->input->post('name');
		$email=$this->input->post('email');
		$number=$this->input->post('number');
		$role=$this->input->post('role');
		$user=$this->input->post('username');
		$lan=$this->input->post('lan');
		$pas=$this->input->post('password');
		$pass=md5($this->input->post('password'));	
		$new = array('name' => $name,
					'email'=>	$email,
					'number'=>	$number,
					);
		/*Calls Model to insert data in User table*/
		$this->model_users->can_reg($new);
		$dat=array(
				'email'=>$this->input->post('email')
				);

			$ses=$this->session->set_userdata($dat);
			$this->load->model('model_users');

		/*Calls model to get id of the last inserted data in User table*/	
			$ids=$this->model_users->getid();
			$i=array(
				'id'=>$ids
				);
			$sess=$this->session->set_userdata($i);
			$log=array('username'=>$user,
						'password'=>$pass,
						'roll_id'=>$role,
						'user_id'=>$ids,
						'lan_id'=>$lan,
				);

		/*Calls Model to enter login details  in Login Table of the corresponding user*/
		$this->model_users->addlogin($log);

		/*Send mail to the registered user with their username nd password*/
		$this->load->library('my_phpmailer');
   
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.bizmail.yahoo.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "hanan@webqua.com";  // user email address
        $mail->Password   = "pmhkabeer";            // password in GMail
        $mail->SetFrom('hanankabir@gmail.com', 'Webqua Solutions');  //Who is sending the email
        $mail->AddReplyTo("hanankabir@gmail.com","Webqua Solutions");  //email address that receives the response
        $mail->Subject    = "Login Details For Webqua Attendance Management";

        $mail->Body      = "Username is ".$user."\nPassword is ".$pas;

        // $mail->msgHTML($body, dirname(__FILE__), true);
        // $mail->AltBody    = "Username is".$user." <br/> Password is".$pass;
        $destino = $email; // Who is addressed the email to
        $mail->AddAddress($destino,$name);

        // $mail->AddAttachment("images/phpmailer.gif");      // some attached files
        // $mail->AddAttachment("images/phpmailer_mini.gif"); // as many as you want
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
        

		/*To View Login Screen if registered successfully*/
		$data['message'] = ' Successfully Registered ...Login In';
		$this->load->view("admin_1/add_user",$data);

		}
		
		
		else 

		{
			/*To View Register Screen if not registered successfully with the message included*/
			$data['message']='Data Not Inserted Successfully';
			$this->load->view("admin_1/page_user_login_1",$data);
		}					

	}

	/*function to unset and destroy  session variables and direct to login screen*/
	public function signout()
	{
		
		$this->session->sess_destroy();
		$this->load->view('admin_1/page_user_login_1');
	}

	/*function to unset and destroy  session variables and direct to login screen*/
	public function previlage()
	{
		$this->load->model('model_users');
		$this->model_users->getid();
		
	}
}
?>
