<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addsheet extends CI_Controller {

	
	public function index()
	{
		$this->load->view('admin_1/starter');
	}

	public function addtime()
	{	
		$this->load->model('model_users');
		$pro['name']=$this->model_users->viewproject();
		$pro['results']=$this->model_users->viewtimesheet();
		$this->load->view('admin_1/add_timesheet',$pro);
	}

	public function newtimesheet()
	{	
		$this->load->model('model_users');
		$pro['name']=$this->model_users->viewproject();

		$pro['user']=$this->model_users->viewuser();
		$this->load->view('admin_1/addviewtimesheet',$pro);
	}

	public function addtimesheet()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('project','project','required');
		$this->form_validation->set_rules('fromdate','fromdate','required');	
		$this->form_validation->set_rules('todate','todate','required');
		$this->form_validation->set_rules('description','description','required');
		if($this->form_validation->run())
		{
			
		$this->load->model('model_users');
		$project= $this->input->post('project');
		$fromdate=$this->input->post('fromdate');
		$todate=$this->input->post('todate');
		$description=$this->input->post('description');
		$user_id=$this->session->userdata('id');
		$rid=$this->session->userdata('rid');
		if($rid=='1')
		{
		$new = array('project_id' => $project,
					'fromdate'=>	$fromdate,
					'todate'=>	$todate,
					'user_id'=>$user_id,
					'description'=>$description,
					);
		}
		elseif ($rid=='2') {
			$new = array('project_id' => $project,
					'fromdate'=>	$fromdate,
					'todate'=>	$todate,
					'user_id'=>$user_id,
					'description'=>$description,
					'approved'=>1,
					);
		}
		else
		{
			$new = array('project_id' => $project,
					'fromdate'=>	$fromdate,
					'todate'=>	$todate,
					'user_id'=>$user_id, 
					'description'=>$description,
					'approved'=>2,
					);

		}
		$this->model_users->addtimesheet($new);

		
		$data['message'] = ' Successfully Added';
		$data['name']=$this->model_users->viewproject();
	
		$data['results']=$this->model_users->viewtimesheet();
		$this->load->view("admin_1/add_timesheet",$data);
	}
	else
	{
		$this->load->model('model_users');
		$data['message'] = ' Not Added';
		$data['name']=$this->model_users->viewproject();
		$data['results']=$this->model_users->viewtimesheet();  
		
		$this->load->view("admin_1/add_timesheet",$data);
	}
}


public function addnew()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('project','project','required');
		$this->form_validation->set_rules('date','date','required');
		$this->form_validation->set_rules('fromdate','fromdate','required');	
		$this->form_validation->set_rules('todate','todate','required');
		$this->form_validation->set_rules('description','description','required');
		if($this->form_validation->run())
		{
			
		$this->load->model('model_users');
		$project= $this->input->post('project');
		
		$date= $this->input->post('date');
		$dt = new DateTime($date);
		$format_date=$dt->format('Y-m-d');

		$fromdate=$this->input->post('fromdate');
		$todate=$this->input->post('todate');
		$description=$this->input->post('description');
		$user_id=$this->input->post('user');
		$new = array('project_id' => $project,
					'Date'=>$format_date,
					'fromdate'=>$fromdate,
					'todate'=>$todate,
					'user_id'=>$user_id,
					'description'=>$description,
					'approved'=>2,
					);
		$this->model_users->addnew($new);

		
		$data['message'] = ' Successfully Added';
		$data['name']=$this->model_users->viewproject();
		$data['user']=$this->model_users->viewuser();
		
		$this->load->view("admin_1/addviewtimesheet",$data);
	}
	else
	{
		$this->load->model('model_users');
		$data['message'] = ' Not Added';
		$data['name']=$this->model_users->viewproject();
		$this->load->view("admin_1/addviewtimesheet",$data);
	}
}

public function addproject()
	{
		$this->load->view('admin_1/add_project');
	}

public function addprojectname()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('project','project','required');
		$this->form_validation->set_rules('language','language','required');
		$this->form_validation->set_rules('duration','Duration','required');
		$this->form_validation->set_rules('cost','Cost','required');
		$this->form_validation->set_rules('add_cost','Additional Cost','required');

		if($this->form_validation->run())
		{
			$this->load->model('model_users');
			
			$data=array(
				'project'=>$this->input->post('project'),
				'language'=>$this->input->post('language'),
				'duration'=>$this->input->post('duration'),
				'cost'=>$this->input->post('cost'),
				'add_cost'=>$this->input->post('add_cost')
				);
			$this->model_users->addproject($data);	
			$data['message'] = ' Successfully Added';
		$this->load->view("admin_1/add_project",$data);
	
		}

	}

public function viewtimesheet()
	{
		$this->load->model('model_users');
		$data['results']=$this->model_users->viewtimesheet();
		$this->load->view('admin_1/view_timesheet',$data);
	}


	public function reviewtimesheet()
	{
		$this->load->model('model_users');
		$data['results']=$this->model_users->reviewtimesheet();
		$this->load->view('admin_1/review_timesheet',$data);
	}

	public function status()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_users');
		$this->model_users->status($sess);
		$this->reviewtimesheet();
	}

public function viewprojectlist()
	{
		$this->load->model('model_users');
		$data['results']=$this->model_users->viewprojectlist();
		$this->load->view('admin_1/projectlist',$data);
	}


	public function approvetimesheet()
	{
		$this->load->model('model_users');
		$data['results']=$this->model_users->approvetimesheet();
		$this->load->view('admin_1/approvetimesheet',$data);
	}

	public function super_status()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_users');
		$this->model_users->super_status($sess);
		$this->approvetimesheet();
	}
 
	public function edit()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_users');
		$data['results']=$this->model_users->edit($sess);
		$data['name']=$this->model_users->viewproject();
		$this->load->view('admin_1/edit',$data);
	}

	public function edit1()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_users');
		$data['results']=$this->model_users->edit($sess);
		$data['name']=$this->model_users->viewproject();
		$this->load->view('admin_1/edit1',$data);
	}

  
	public function edit2()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_users');
		$data['results']=$this->model_users->edit($sess);
		$data['name']=$this->model_users->viewproject();
		$this->load->view('admin_1/edit2',$data);
	}

//bindya
	public function edit3()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_users');
		$data['results']=$this->model_users->edit3($sess);
		/*$data['name']=$this->model_users->viewprojectlist();*/
		$this->load->view('admin_1/edit3',$data);
	}

	public function delete()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_users');
		$this->model_users->deletesheet($sess);
		
		redirect($_SERVER['HTTP_REFERER']); 
	}


public function editsheet()
	{
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('project','project','required');
		$this->form_validation->set_rules('fromdate','fromdate','required');	
		$this->form_validation->set_rules('todate','todate','required');
		$this->form_validation->set_rules('description','description','required');
		if($this->form_validation->run())
		{
			
		$this->load->model('model_users');
		$project= $this->input->post('project');
		$fromdate=$this->input->post('fromdate');
		$todate=$this->input->post('todate');
		$description=$this->input->post('description');
		
		$i=$this->input->post('id');
		$new = array('project_id' => $project,
					'fromdate'=>$fromdate,
					'todate'=>$todate,
					'description'=>$description,
					);
		$this->model_users->editsheet($new,$i);

		$this->reviewtimesheet();

	}
}

public function editsheet1()
	{
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('project','project','required');
		$this->form_validation->set_rules('fromdate','fromdate','required');	
		$this->form_validation->set_rules('todate','todate','required');
		$this->form_validation->set_rules('description','description','required');
		if($this->form_validation->run())
		{
			
		$this->load->model('model_users');
		$project= $this->input->post('project');
		$fromdate=$this->input->post('fromdate');
		$todate=$this->input->post('todate');
		$description=$this->input->post('description');
		
		$i=$this->input->post('id');
		$new = array('project_id' => $project,
					'fromdate'=>$fromdate,
					'todate'=>$todate,
					'description'=>$description,
					);
		$this->model_users->editsheet($new,$i);
		$this->viewtimesheet();

	}
}

public function editsheet2()
	{
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('project','project','required');
		$this->form_validation->set_rules('fromdate','fromdate','required');	
		$this->form_validation->set_rules('todate','todate','required');
		$this->form_validation->set_rules('description','description','required');
		if($this->form_validation->run())
		{
			
		$this->load->model('model_users');
		$project= $this->input->post('project');
		$fromdate=$this->input->post('fromdate');
		$todate=$this->input->post('todate');
		$description=$this->input->post('description');
		
		$i=$this->input->post('id');
		$new = array('project_id' => $project,
					'fromdate'=>$fromdate,
					'todate'=>$todate,
					'description'=>$description,
					);
		$this->model_users->editsheet($new,$i);
		$this->approvetimesheet();

	}
}
//bindya
public function editsheet3()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_rules('language','language','required');
		$this->form_validation->set_rules('project','project','required');
		$this->form_validation->set_rules('duration','duration','required');	
		$this->form_validation->set_rules('cost','cost','required');
		$this->form_validation->set_rules('add_cost','add_cost','required');
		if($this->form_validation->run())
		{
		$this->load->model('model_users');
		$language= $this->input->post('language');
		$project=$this->input->post('project');
		$duration=$this->input->post('duration');
		$cost=$this->input->post('cost');
		$add_cost=$this->input->post('add_cost');

		
		$i=$this->input->post('id');
		$new = array('language' => $language,
					'project'=>$project,
					'duration'=>$duration,
					'cost'=>$cost,
					'add_cost'=>$add_cost,
					);
		$this->model_users->editsheet4($new,$i);
		$this->viewprojectlist();
		/*$data['results']=$this->model_users->viewprojectlist();
		$this->load->view('admin_1/projectlist',$data);*/

	}
	
}



	public function salarysheet()
	{	
		$this->load->model('model_users');
		

		$pro['user']=$this->model_users->viewuser();
		$this->load->view('admin_1/add_salary',$pro);
	}

public function addsalary()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_rules('user','user','required');
		$this->form_validation->set_rules('salary','salary','required');
       /* $this->form_validation->set_rules('sph','sph','required');*/
		// $this->form_validation->set_rules('date','date','required');

		if($this->form_validation->run())
		{
			$this->load->model('model_users');
            $user_id=$this->input->post('user');
            $salary=$this->input->post('salary');
			$sph=$salary*'12'/'52'/'40';
	
		
		$data=array('user_id'=>$this->input->post('user'),
                   'salary'=>$this->input->post('salary'),
                   'sph'=>$sph);
        
		
	
		$this->model_users->addsalary($data);

		
		$data['message'] = 'Successfully Added';
		
		$data['user']=$this->model_users->viewuser();
		$data['sph'] = $this->model_users->viewsph();
		$this->load->view("admin_1/add_salary",$data);
	}
	
}

public function AddProjectList()
	{
      
       $this->load->view('admin_1/addprojectlist');
	}


public function addprojectlistname()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('project_name','Project','required');
		$this->form_validation->set_rules('duration','Duration','required');
		$this->form_validation->set_rules('cost','Cost','required');
		$this->form_validation->set_rules('spent','Spent','required');
		if($this->form_validation->run())
		{
			$this->load->model('model_users');
			
			$data=array(
				'project_name'=>$this->input->post('project_name'),
				'duration'=>$this->input->post('duration'),
				'cost'=>$this->input->post('cost'),
				'spent'=>$this->input->post('spent')
				);
			$this->model_users->addprojectlist($data);	
			$data['message'] = ' Successfully Added';
		$this->load->view("admin_1/addprojectlist",$data);
	
		}

	}

public function ProjectList()
	{

       $this->load->view('admin_1/projectlist');
	}

public function ProjectDetails()
{
	$sess = $this->uri->segment(3);
		$this->load->model('model_staff');
		$data['results']=$this->model_staff->projectdetail($sess);
		$data['result'] =$this->model_staff->projectdet($sess);
		// $ids=$this->model_staff->projectdet($sess);
		// $i=array(
				// 'id'=>$ids->user_id,
				// 'name'=>$ids->name,
				// );
		$data['res']=$this->model_staff->projectdeta($sess);
		
	$this->load->view("admin_1/projectdetails",$data);
}






}