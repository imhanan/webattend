<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adduser extends CI_Controller {

	
	public function index()
	{
		$this->add_user();
	}


	public function add_user()
	{
		$this->load->view('admin_1/add_user');
	}

	public function viewuser()
	{
		$this->load->model('model_staff');
		$data['results']=$this->model_staff->view_user();
		$this->load->view('admin_1/view_user',$data);
	}

	public function edit()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_staff');
		$data['results']=$this->model_staff->edit($sess);
		$this->load->view('admin_1/edituser',$data);
	}

	public function edituser()
	{
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('email','email','required');	
		$this->form_validation->set_rules('number','number','required');
		$this->form_validation->set_rules('role','role','required');
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');	

		if($this->form_validation->run())
		{
			
		$this->load->model('model_staff');
		$name= $this->input->post('name');
		$email=$this->input->post('email');
		$number=$this->input->post('number');
		$role=$this->input->post('role');
		$user=$this->input->post('username');
		$lan=$this->input->post('lan');
		$pass=md5($this->input->post('password'));	
		$new = array('name' => $name,
					'email'=>	$email,
					'number'=>	$number,
					);
		$i=$this->input->post('id');
		$log=array('username'=>$user,
						'password'=>$pass,
						'roll_id'=>$role,
						'user_id'=>$i,
						'lan_id'=>$lan,
				);
		$this->model_staff->edituser($new,$i,$log);

		$this->viewuser();

	}
}

public function delete()
	{
		$sess = $this->uri->segment(3);
		$this->load->model('model_staff');
		$this->model_staff->deleteuser($sess);
		
		redirect($_SERVER['HTTP_REFERER']); 
	}

public function add_time()
    {
        $this->ViewUserTime();

    }


public function ViewUserTime()
	{
		$this->load->model('model_staff');
		$data['result']=$this->model_staff->ViewTime();
		$this->load->view('admin_1/salary',$data);
	}


}



	