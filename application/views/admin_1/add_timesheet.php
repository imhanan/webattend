<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url('Site/home');?>">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                   
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    
                       <!--  <h3 class="page-title"> Form Layouts
                        <small>form layouts</small>
                    </h3> -->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                
                                <?php if (isset($message)) { ?>
<CENTER><h5 style="color:green;"><?php echo $message;?></h5></CENTER><br>
<?php } ?>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title" >
                                                <div class="caption" style="padding: 11px 350px 9px">
                                                    <i class="fa fa-gift"></i>Time Sheet For <?php
                                                         date_default_timezone_set('Asia/Calcutta');echo date('d-M-Y');
                                                        ?> </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="javascript:;" class="expand"> </a>
                                                    <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a> -->
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <!-- <a href="javascript:;" class="remove"> </a> -->
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('Addsheet/addtimesheet');?>" class="form-horizontal" method="post">
                                                    <div class="form-body">
                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label">Project Name</label>
                                                            <div class="col-md-4">
                                                               
                                                                <select type="text" name="project"class="form-control ">
                                                                     <?php
                                                                foreach ($name as $key => $value) {
                                                                    ?>
                                                                <option value="<?= $value->id; ?>" ><?= $value->project; ?></option>
                                                              
                                                                <?php
                                                                }
                                                                ?>
                                                                  </select> 
                                                            </div>
                                                        </div>
                                                        
                                                            <div class="form-group">
                                                <label class="control-label col-md-3">From Time</label>
                                                <div class="col-md-2">
                                                    <div class="input-icon">
                                                        <i class="fa fa-clock-o"></i>
                                                         <input type="text" class="form-control timepicker timepicker-no-seconds" name="fromdate"> </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">To Time</label>
                                                <div class="col-md-2">
                                                    <div class="input-icon">
                                                        <i class="fa fa-clock-o"></i>
                                                         <input type="text" class="form-control timepicker timepicker-no-seconds" name="todate">
                                                         </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Description</label>
                                                            <div class="col-md-4">
                                                                <div class="input-icon">
                                                                    
                                                                   <textarea class="form-control " rows="4" name="description" id="description" required></textarea>
                                                            </div>
                                                        </div>

                                            </div>            
                                             <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>           
                                                       
                                                        
                                                    
                                                    
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                         </div>
                                        </div>


                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>

 <div class="row">
<div class="col-md-12">
<div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-comments"></i>Todays Time Sheet</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="expand"> </a>
                                        <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a> -->
                                        <a href="javascript:;" class="reload"> </a>
                                        <!-- <a href="javascript:;" class="remove"> </a> -->
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                   
                                                    <th> Project</th>
                                                    <th> Date</th>
                                                    <th> From Time</th>
                                                    <th> To Time</th>
                                                    <th> Description</th>
                                                    <th> Status</th>
                                                    <th> Change</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                                foreach ($results as $key => $value) {

                                                    $dat=$value->Date;
                                                      $dc=date('Y-m-d');
                                                      if($dat==$dc)
                                                      {
                                                    
                                                    ?> 
                                                <tr>

                                                      
                                                    
                                                   
                                                    <td> <?= $value->project; ?> </td>
                                                    <td> <?= $value->Date; ?></td>
                                                    <td> <?= $value->fromdate; ?></td>
                                                    <td><?= $value->todate; ?></td>
                                                    <td><?= $value->description  ; ?></td>
                                                    <?php
                                                    if($value->approved=='0')
                                                    {
                                                      ?>
                                                      <td><span class="label label-sm label-warning"> Not Approved</span</td>

                                                      
                                                     <?php
                                                     }
                                                     elseif ($value->approved=='1') {
                                                        ?>
                                                        <td> <span class="label label-sm label-info"> Pending</span></td>
                                                        <?php
                                                      }
                                                       elseif ($value->approved=='2') {
                                                        ?>
                                                        <td> <span class="label label-sm label-success"> Approved</span></td>
                                                        <?php
                                                      }
                                                     date_default_timezone_set('Asia/Calcutta');
                                                     $dat=$value->Date;
                                                      $dc=date('Y-m-d');
                                                      if($dat==$dc or $value->roll_id='3')
                                                      {
                                                      ?>

                                                      <td><a href="<?php echo base_url() ?>Addsheet/edit1/<?php echo$value->timesheet_id; ?>" class="btn btn-outline btn-circle btn-sm purple">
                                                            <i class="fa fa-edit"></i> Edit </a></td>
                                                            <td>
                                                <a class="btn btn-outline btn-circle red btn-sm blue" data-toggle="modal" href="#small-<?php  echo$value->timesheet_id; ?>"><i class="fa fa-trash-o"></i>  Delete </a>
                                            </td>
                                                <div class="modal fade bs-modal-sm" id="small-<?php  echo$value->timesheet_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Delete</h4>
                                                </div>
                                                <div class="modal-body"> Are You Sure?</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green"><a href="<?php echo base_url() ?>Addsheet/delete/<?php echo$value->timesheet_id; ?>" >
                                                            Delete </a></button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                                           
                                                            <?php
                                                        }
                                                       
                                                            ?>
                                                </tr>
                                                <?php
                                            }
                                                }
                                                ?>
                                                
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                            </div>
                             </div>
                            </div>


                </div>
                </div>

            <?php
include('footer.php');
?>