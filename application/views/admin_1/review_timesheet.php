<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Blank Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Page Layouts</span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <h3 class="page-title"> 
                        <small></small>
                    </h3>
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Review Time Sheet Details</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Option 1</a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;">Option 2</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Option 3</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Option 4</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No </th>
                                                     <th> Name</th>
                                                    <th> Project</th>
                                                    <th> Date</th>
                                                    <th> From Time</th>
                                                    <th> To Time</th>
                                                    <th> Description</th>
                                                     <th> Status</th>
                                                     <th> Change</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                                foreach ($results as $key => $value) {
                                                    ?>
                                                <tr>
                                                    <td > <?= ++$key ?> </td>
                                                    <td> <?= $value->name; ?> </td>
                                                    <td> <?= $value->project; ?> </td>
                                                    <td> <?= $value->Date; ?> </td>
                                                    <td> <?= $value->fromdate; ?></td>
                                                    <td><?= $value->todate; ?></td>
                                                    <td><?= $value->description  ; ?></td>
                                                    <?php
                                                    if($value->approved=='0')
                                                    {
                                                      ?>
                                                      <td><a class=" input-circle btn btn-primary" href="<?php echo base_url() ?>Addsheet/status/<?php echo$value->timesheet_id; ?>" role="button">APPROVE</a> <a class="input-circle btn btn-danger" href="" role="button">DISAPPROVE</a></td>

                                                      
                                                     <?php
                                                     }
                                                     elseif ($value->approved=='1') {
                                                        ?>
                                                        <td> <span class="label label-sm label-info"> Pending</span></td>
                                                        <?php
                                                      }
                                                       elseif ($value->approved=='2') {
                                                        ?>
                                                        <td> <span class="label label-sm label-success"> Approved</span></td>
                                                        <?php
                                                      }
                                                      ?>
                                                    <td><a href="<?php echo base_url() ?>Addsheet/edit/<?php echo$value->timesheet_id; ?>" class="btn btn-outline btn-circle btn-sm purple">
                                                            <i class="fa fa-edit"></i> Edit </a></td>

                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <?php
include('footer.php');
?>