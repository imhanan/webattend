<?php
include('header.php');
?>
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url('Site/home');?>">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <!-- <h3 class="page-title">
                        <small>blank page layout</small>
                    </h3> -->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                        
   <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Bordered Table</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> SECTION
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="<?php echo base_url('Adduser/ViewUserTime');?>">PHP</a>
                                                </li>
                                                
                                                <li>
                                                    <a href="<?php echo base_url('Adduser/ViewUserTime');?>">Android</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('Adduser/ViewUserTime');?>">Design</a>
                                                </li>
                                               
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th> No </th>
                                                    <th> User </th>
                                                    <th> Project </th>

                                                    <th> Time </th>
                                                    <th> Total Time</th>
                                                    <!-- <th> Username </th>
                                                    <th> Status </th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                              
                                                	<?php
                                                foreach ($result as $key => $value) {
                                                    ?>
                                                      <tr>
                                                    <td > <?= ++$key; ?> </td>
                                                    <!-- <td rowspan="2"> 1 </td> -->
                                                    <td> <?= $value->name; ?> </td>
                                                    <td> <?= $value->project; ?> </td>

                                                    <td> 
                                                    
                                                   <?php
                                                    $fromdate = $value ->fromdate ;
                                                     $todate = $value ->todate ;
                                                    $date_a = new DateTime($fromdate);
                                                    $date_b = new DateTime($todate);

                                                   $interval = date_diff($date_a,$date_b);

                                                    echo $interval->format('%h:%i:%s');
                                                    ?>

                                                </td>
                                                <td>
                                                     <?php
                                                        $fromdate = $value ->fromdate ;
                                                     $todate = $value ->todate ;
                                                    $date_a = new DateTime($fromdate);
                                                    $date_b = new DateTime($todate);

                                                        $diff = $date_b->diff($date_a);

                                                         $hours = $diff->h;
                                                          $hours = $hours + ($diff->days*24);

                                                        echo $hours;
                                                        ?>
                                                    </td>
                                                </tr>
                                                
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
      
         </div>
      </div>
      <?php
include('footer.php');
?>