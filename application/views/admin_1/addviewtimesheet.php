<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url('Site/home');?>">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                   
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    
                       <!--  <h3 class="page-title"> Form Layouts
                        <small>form layouts</small>
                    </h3> -->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                
                                <?php if (isset($message)) { ?>
<CENTER><h5 style="color:green;"><?php echo $message;?></h5></CENTER><br>
<?php } ?>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title" >
                                                <div class="caption" style="padding: 11px 350px 9px">
                                                    <i class="fa fa-gift"></i>Time Sheet 
                                                         </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('Addsheet/addnew');?>" class="form-horizontal" method="post">
                                                    <div class="form-body">

                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label">Project Name</label>
                                                            <div class="col-md-4">
                                                                
                                                                <select type="text" name="project"class="form-control ">
                                                                    <?php
                                                                foreach ($name as $key => $value) {
                                                                    ?>
                                                                <option value="<?= $value->id; ?>" ><?= $value->project; ?></option>
                                                                
                                                                <?php
                                                                }
                                                                ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                     <label class="col-md-3 control-label">Staff</label>
                                                        <div class="col-md-4">
                                                                
                                                                <select type="text" name="user"class="form-control ">
                                                                    <?php
                                                                foreach ($user as $key => $value) {
                                                                    ?>
                                                                <option value="<?= $value->id; ?>" ><?= $value->name; ?></option>
                                                                
                                                                <?php
                                                                }
                                                                ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                         <div class="form-group">
                                                <label class="control-label col-md-3">Date</label>
                                                <div class="col-md-2">
                                                    <input class="form-control  form-control-inline input-medium date-picker" name="date" size="16" type="text" value="" />
                                                   
                                                </div>
                                            </div>
                                                        
                                                            <div class="form-group">
                                                <label class="control-label col-md-3">From Time</label>
                                                <div class="col-md-2">
                                                    <div class="input-icon">
                                                        <i class="fa fa-clock-o"></i>
                                                        <input type="text" class="form-control timepicker timepicker-no-seconds" name="fromdate"> </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">To Time</label>
                                                <div class="col-md-2">
                                                    <div class="input-icon">
                                                        <i class="fa fa-clock-o"></i>
                                                        <input type="text" class="form-control timepicker timepicker-no-seconds" name="todate"> </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Description</label>
                                                            <div class="col-md-4">
                                                                <div class="input-icon">
                                                                    
                                                                   <textarea class="form-control " rows="4" name="description" id="description" required></textarea>
                                                            </div>
                                                        </div>

                                            </div>            
                                             <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>           
                                                       
                                                        
                                                    
                                                    
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                         </div>
                                        </div>


                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>



                </div>
                </div>

            <?php
include('footer.php');
?>