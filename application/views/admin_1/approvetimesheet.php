<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Blank Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Page Layouts</span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <h3 class="page-title"> 
                        <small></small>
                    </h3>
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Approve Time Sheet Details</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Option 1</a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;">Option 2</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Option 3</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Option 4</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No </th>
                                                     <th> Name</th>
                                                    <th> Project</th>
                                                    <th> Date</th>
                                                    <th> From Time</th>
                                                    <th> To Time</th>
                                                    <th> Description</th>
                                                   
                                                     <th colspan="2"> Status</th>
                                                     
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                                foreach ($results as $key => $value) {
                                                    ?>
                                                <tr>
                                                    <td > <?= ++$key ?> </td>
                                                    <td> <?= $value->name; ?> </td>
                                                    <td> <?= $value->project; ?> </td>
                                                    <td> <?= $value->Date; ?></td>
                                                    <td> <?= $value->fromdate; ?></td>
                                                    <td><?= $value->todate; ?></td>
                                                    <td><?= $value->description  ; ?></td>
                                                    <?php
                                                    if($value->approved=='0')
                                                    {
                                                      ?>
                                                      <td> <span class="label label-sm label-warning"> Approval Pending </span></td>

                                                      
                                                     <?php
                                                     }
                                                     elseif ($value->approved=='1') {
                                                        ?>
                                                        <td> <span class="label label-sm label-info "> Pending </span></td>
                                                         <td><a class=" input-circle btn btn-primary" href="<?php echo base_url() ?>Addsheet/super_status/<?php echo$value->timesheet_id; ?>" role="button">APPROVE</a> </td>
                                                        <?php
                                                      }
                                                       elseif ($value->approved=='2') {
                                                        ?>
                                                        <td> <span class="label label-sm label-success"> Approved</span></td>
                                                        <?php
                                                      }
                                                      ?>
                                                      <td><a href="<?php echo base_url() ?>Addsheet/edit2/<?php echo$value->timesheet_id; ?>" class="btn btn-outline btn-circle btn-sm purple">
                                                            <i class="fa fa-edit"></i> Edit </a></td>
                                                           <td>
                                                <a class="btn btn-outline btn-circle red btn-sm blue" data-toggle="modal" href="#small-<?php  echo$value->timesheet_id; ?>"><i class="fa fa-trash-o"></i>  Delete </a>
                                            </td>
                                                </tr>
                                                  <div class="modal fade bs-modal-sm" id="small-<?php  echo$value->timesheet_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Delete</h4>
                                                </div>
                                                <div class="modal-body"> Are You Sure?</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green"><a href="<?php echo base_url() ?>Addsheet/delete/<?php echo$value->timesheet_id; ?>" >
                                                            Delete </a></button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                                <?php
                                                }
                                                ?>
                                                
                                               
                                            </tbody>
                                        </table>
                                      
                                    </div>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
            </div>
            </div>
            <?php
include('footer.php');
?>