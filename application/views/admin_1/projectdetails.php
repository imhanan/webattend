<?php
include('header.php');
?>
<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="background:#eef1f5 !important;" >
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler"> </div>
                        <div class="toggler-close"> </div>
                        <div class="theme-options">
                            <div class="theme-option theme-colors clearfix">
                                <span> THEME COLOR </span>
                                <ul>
                                    <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"> </li>
                                    <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"> </li>
                                    <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"> </li>
                                    <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"> </li>
                                    <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"> </li>
                                    <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"> </li>
                                </ul>
                            </div>
                            <div class="theme-option">
                                <span> Theme Style </span>
                                <select class="layout-style-option form-control input-sm">
                                    <option value="square" selected="selected">Square corners</option>
                                    <option value="rounded">Rounded corners</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Layout </span>
                                <select class="layout-option form-control input-sm">
                                    <option value="fluid" selected="selected">Fluid</option>
                                    <option value="boxed">Boxed</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Header </span>
                                <select class="page-header-option form-control input-sm">
                                    <option value="fixed" selected="selected">Fixed</option>
                                    <option value="default">Default</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Top Menu Dropdown</span>
                                <select class="page-header-top-dropdown-style-option form-control input-sm">
                                    <option value="light" selected="selected">Light</option>
                                    <option value="dark">Dark</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Sidebar Mode</span>
                                <select class="sidebar-option form-control input-sm">
                                    <option value="fixed">Fixed</option>
                                    <option value="default" selected="selected">Default</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Sidebar Menu </span>
                                <select class="sidebar-menu-option form-control input-sm">
                                    <option value="accordion" selected="selected">Accordion</option>
                                    <option value="hover">Hover</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Sidebar Style </span>
                                <select class="sidebar-style-option form-control input-sm">
                                    <option value="default" selected="selected">Default</option>
                                    <option value="light">Light</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Sidebar Position </span>
                                <select class="sidebar-pos-option form-control input-sm">
                                    <option value="left" selected="selected">Left</option>
                                    <option value="right">Right</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span> Footer </span>
                                <select class="page-footer-option form-control input-sm">
                                    <option value="fixed">Fixed</option>
                                    <option value="default" selected="selected">Default</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Blank Page</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Page Layouts</span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    
                   
                    
                    
                    
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> Project Details
                        <!--<small>blank page layout</small>-->
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                            
                               <!-- BEGIN PORTLET--> 
                             <?php
                             foreach ($results as $key => $value) {
                                                
                                                    ?> 
                               <div class="col-sm-12">
                               
                               <div class=" col-sm-6">
                           <!-- <div class="portlet light bordered">-->
                                
                               
                                <div class="portlet-body">
                                   
                                    <div class="margin-top-10 margin-bottom-10 clearfix">
                                        <table class="table table-bordered table-striped">
                                            
                                            <tr>
                                                <td> Project Name</td>
                                                <td><?=$value->project;?>-<?=$value->language;?> </td>
                                                
                                            </tr>
                                            <tr>
                                                <td> Project Duration</td>
                                                 <td> <?=$value->duration;?></td>
                                                
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                           <!-- </div>-->
                           
                    </div>
                    
                    
                    <div class=" col-sm-6">
                           <!-- <div class="portlet light bordered">-->
                                
                               
                                <div class="portlet-body">
                                   
                                    <div class="margin-top-10 margin-bottom-10 clearfix">
                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <td> Project Cost </td>
                                                 <td> <?=$value->cost;?></td>
                                               <!-- <td>
                                                    <div id="pulsate-regular" style="padding:5px;"> Repeating Pulsate </div>
                                                </td>-->
                                            </tr>

                                            <?php
                }

                    ?>
                                            
                                            <?php
                                  $total=0;
                             foreach ($result as $key => $value) {
                                     $resul="0:0:0";
                                        foreach ($res as $key => $values) {

                                             $name=$value->name;
                                $cname=$values->name;

                                if($name==$cname)
                                {
                                     $resu="0:0:0";
                                     $secs="0:0:0";

                                    $fromdate = $values ->fromdate ;
                                                     $todate = $values ->todate ;
                                                    $date_a = new DateTime($fromdate);
                                                    $date_b = new DateTime($todate);

                                                   $interval = date_diff($date_a,$date_b);
                                                   $in=$interval->format('%h:%i:%s');
                                                   // $inte=$inte+$in;
                                                   // $interv=$interv+$inte;


                                                   $secs = strtotime($in)-strtotime("00:00:00");
                                                $resu= date("H:i",strtotime($resu)+$secs);
                                                 $secss = strtotime($resu)-strtotime("00:00:00");
                                                $resul= date("H:i",strtotime($resul)+$secss);

                                               }
                                               }

                                               $sph=$value->sph;
                                                $time=explode(":",$resul);/*To get the minute value*/
                                                $minuteamount=($sph*$time[1])/60;/*To get the minute amount*/
                                               $spent=$sph*$resul;/*To get the hour amount*/
                                               $sp=$spent+$minuteamount;/*To get the total of hour and minute amount*/
                                               $total=$total+$sp;
                                               
                                           }
                                                    ?> 

                                                
                                                    <tr>
                                                <td> Spent</td>
                                                <td><?=$total;?></td>
                                                <!--<td>
                                                    <div id="pulsate-regular" style="padding:5px;"> Repeating Pulsate </div>
                                                </td>-->
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                           <!-- </div>-->
                           
                    </div>
                    </div>
                    
                   
                     
                   
                    
                    
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
                  
                   <div class="col-sm-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i> Time sheet </div>
                                    <div class="tools">
                                         <?php
                                         $resu="0:0:0";
                             foreach ($res as $key => $value) {
                                                $fromdate = $value ->fromdate ;
                                                     $todate = $value ->todate ;
                                                    $date_a = new DateTime($fromdate);
                                                    $date_b = new DateTime($todate);


                                                   $interval = date_diff($date_a,$date_b);
                                                   $in=$interval->format('%h:%i:%s');
                                                  
                                                   // $inte=$inte+$in;
                                                   

                                                   $secs = strtotime($in)-strtotime("00:00:00");
                                                $resu= date("H:i:s",strtotime($resu)+$secs);

                                               }
                                                
                                                    ?> 
                                        <p class="pull-left" style=" margin:0px"><?=$resu;?></p>
                                        <a href="javascript:;" class="expand"> </a>
                                      
                                    </div>
                                </div>
                                
                                
                                 <?php
                                 
                             foreach ($result as $key => $value) {
         
                                                    ?> 
                                <div class="portlet-body flip-scroll" >
                                    
                                    
                                    <div class="portlet box default">

                                        <?php
                                         $resul="0:0:0";
                                        foreach ($res as $key => $values) {

                                             $name=$value->name;
                                $cname=$values->name;

                                if($name==$cname)
                                {
                                     $resu="0:0:0";
                                     $secs="0:0:0";

                                    $fromdate = $values ->fromdate ;
                                                     $todate = $values ->todate ;
                                                    $date_a = new DateTime($fromdate);
                                                    $date_b = new DateTime($todate);

                                                   $interval = date_diff($date_a,$date_b);
                                                   $in=$interval->format('%h:%i:%s');
                                                   // $inte=$inte+$in;
                                                   // $interv=$interv+$inte;


                                                   $secs = strtotime($in)-strtotime("00:00:00");
                                                $resu= date("H:i:s",strtotime($resu)+$secs);
                                                 $secss = strtotime($resu)-strtotime("00:00:00");
                                                $resul= date("H:i:s",strtotime($resul)+$secss);

                                               }
                                               }


                                        ?>
                                        
                                          
                                <div class="portlet-title">
                                    <div class="caption">


                                        <i class="fa fa-cogs"></i><?=$value->name;?></div>

                                        
                                       
                                    <div class="tools">

                                        
                                        <p class="pull-left" style=" margin:0px"><?=$resul?></p>

                                       
                                        <a href="javascript:;" class="expand"> </a>
                                      
                                    </div>
                                </div>
                           

                               

                                
                                                                
                                 
                                
                                
                                   
                                     <div class="portlet-body flip-scroll" style="display:none;">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th width="20%"> Date </th>
                                                <th> Work </th>
                                                <th class="numeric"> NO:Hours </th>
                                                <th class="numeric"> Time </th>
                                                
                                            </tr>
                                        </thead>
                                         <?php
                                         $interv=0;
                                         foreach ($res as $key => $values) {

                                $name=$value->name;
                                $cname=$values->name;

                                if($name==$cname)
                                {
                                    $inte=0;
                                    $fromdate = $values ->fromdate ;
                                                     $todate = $values ->todate ;
                                                    $date_a = new DateTime($fromdate);
                                                    $date_b = new DateTime($todate);

                                                   $interval = date_diff($date_a,$date_b);
                                                   $in=$interval->format('%h:%i:%s');
                                                   $inte=$inte+$in;
                                                   // $inte=$inte->format('%h:%i:%s');
                                                   $interv=$interv+$inte;

                                                


                                        ?>
                                       <tbody>
                                            <tr>
                                                 
                                                <td> <?=$values->Date;?></td>
                                                <td> <?=$values->description;?></td>
                                                <td class="numeric"><?=$in;?></td>
                                                <td class="numeric"> <?=$values->fromdate;?>-<?=$values->todate;?></td>

                                               
                                            </tr>
                                            
                                        </tbody>

                                         <?php

                                 }
                             }
                                ?>
                                    </table>
                                   
                                </div>

                               

                                   
                                
                            </div>
                          
                                <!--***********-->
                               
                                </div>
                                 <?php
                            
                            }

                                ?>
                                
                            </div>
                            </div>

                            


                            <!-- END SAMPLE TABLE PORTLET-->
                          
                        </div>
                    </div>
            <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2014 &copy; Metronic by keenthemes.
                <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url('assets/global/plugins/jquery.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/uniform/jquery.uniform.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url('assets/global/scripts/app.min.js');?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url('assets/layouts/layout/scripts/layout.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/layout/scripts/demo.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/global/scripts/quick-sidebar.min.js');?>" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>