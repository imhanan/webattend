<!-- bindya -->
<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url('Site/home');?>">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                   
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    
                       <!--  <h3 class="page-title"> Form Layouts
                        <small>form layouts</small>
                    </h3> -->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                
                                <?php if (isset($message)) { ?>
<CENTER><h5 style="color:green;"><?php echo $message;?></h5></CENTER><br>
<?php } ?>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title" style="text-align:center">
                                                <div class="caption" style="text-align:center">
                                                    <i class="fa fa-gift"></i>Time Sheet </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="javascript:;" class="expand"> </a>

                                                    <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a> -->
                                                    <a href="javascript:;" class="reload"> </a>
                                                   <!--  <a href="javascript:;" class="remove"> </a> -->
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('Addsheet/editsheet3');?>" class="form-horizontal" method="post">
                                                    <div class="form-body">
                                                        <?php foreach ($results as $key => $values) {
                                                           
                                                               $id=$values->id;
                                                               form_hidden('id',$id);
                                                        ?>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Section</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control " type="text" placeholder="Section" name="language" value="<?= $values->language; ?>" /> 
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Project Name</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control " type="text" placeholder="Project Name" name="project" value="<?= $values->project; ?>" /> 
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Duration</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control " type="text" placeholder="Duration" name="duration" value="<?= $values->duration; ?>" /> 
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Project Cost</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control " type="text" placeholder="Project Cost" name="cost" value="<?= $values->cost; ?>" /> 
                                                            </div>
                                                        </div>

                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label">Additional Cost</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control " type="text" placeholder="Additional Cost" name="add_cost" value="<?= $values->add_cost; ?>" /> 
                                                            </div>
                                                        </div>

                                                         
                                            
                                                            
                                            <?php } ?>      
                                            </div>       
                                             <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <input type="hidden" name="id" value="<?php echo $values->id;?>"/> 
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>           
                                                       
                                                        
                                                    
                                                    
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                         </div>
                                        </div>

                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
                </div>
                </div>

            <?php
include('footer.php');
?>