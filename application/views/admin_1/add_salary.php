<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url('Site/home');?>">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <!-- <h3 class="page-title">
                        <small>blank page layout</small>
                    </h3> -->
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                               <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                
                                <?php if (isset($message)) { ?>
<CENTER><h5 style="color:green;"><?php echo $message;?></h5></CENTER><br>
<?php } ?>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title" >
                                                <div class="caption" >
                                                    <i class="fa fa-gift"></i>Salary Details</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="javascript:;" class="expand"> </a>
                                                    <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a> -->
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <!-- <a href="javascript:;" class="remove"> </a> -->
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('Addsheet/addsalary');?>" class="form-horizontal register-form" method="post">
                                                    <div class="form-body">
                                                        
                                                        
                                                            
                                             <div class="form-group">
                                                            <label class="col-md-3 control-label">Users</label>
                                                            <div class="col-md-4">
                                                                
                                                                <select type="text" name="user" class="form-control ">
                                                                    <?php
                                                                foreach ($user as $key => $value) {
                                                                    ?>
                                                                    <!-- <option value="" >Select Users</option> -->
                                                                <option value="<?= $value->id; ?>" ><?= $value->name; ?></option>
                                                                
                                                                <?php
                                                                }
                                                                ?>
                                                                </select>
                                                            </div>
                                                        </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Salary</label>
                        <div class="col-md-4">
                                                               
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Salary" name="salary" />
                        </div>
                </div>
               <!--  <div class="form-group">
                    <label class="col-md-3 control-label">Salary per hour</label>
                     <div class="col-md-4">
                        <p class="form-control-static"> <?php echo $sph ;?></p>
                    </div>
                </div> -->
                <div class="form-group margin-top-20 margin-bottom-20">
                    
                    <div id="register_tnc_error"> </div>
                </div>
                </div>            
                                             <!-- <div class="form-actions"> -->
                                                        <div class="row">
                                                            <div class="col-md-offset-4 col-md-9">
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->  
                                                    <div class="form-group margin-top-20 margin-bottom-20">
                    
                    <div id="register_tnc_error"> </div>
                </div>         
                                                       
                                                        
               
                                                    
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                         </div>
                                        </div>


                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>


           
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <?php
include('footer.php');
?>