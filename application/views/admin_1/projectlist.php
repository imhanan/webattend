<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url('Site/home');?>">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                    <h3 class="page-title"> 
                        <small></small>
                    </h3>
                    <div class="row">
                   <div class="col-md-12">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-dark"></i>
                                        <span class="caption-subject font-dark bold uppercase">Project Details</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group">
                                            <a class="btn dark btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Option 1</a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;">Option 2</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Option 3</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Option 4</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No </th>
                                                    <th> Section</th>
                                                    <th> Project Name</th>
                                                    <th> Project Duration</th>
                                                    <th> Project Cost</th>
                                                    <th> Additional Cost</th>
                                                    <th> Total Spent</th>
                                                    <th> Status</th>
                                                    <th> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($results as $key => $value) {
                                                    ?>
                                                <tr>
                                                    <td > <?= ++$key; ?> </td>
                                                    <td> <?= $value->language; ?> </td>
                                                    <td> <?= $value->project; ?> </td>
                                                    <td> <?= $value->duration; ?></td>

                                                    <td> <?= $value->cost; ?></td>
                                                    <td> <?= $value->add_cost; ?></td>
                                                    
                                                    <td></td>

                                                <td>
                                                    <span class="label label-sm "> <a href="<?php echo base_url() ?>Addsheet/edit3/<?php echo$value->id; ?>">Edit</a></span>
                                                    <span class="label label-sm label-warning"> <a href="<?php echo base_url()?>Addsheet/ProjectDetails/<?php echo$value->id; ?>">View Project</a></span>
                                                    </td>
                                                <?php
                                                        }
                                                            ?>
                                                </tr>

                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div
                                                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                        </div>
                <!-- END CONTENT BODY -->
  

            </div>
            </div>
            <?php
include('footer.php');
?>