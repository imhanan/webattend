<?php
include('header.php');
?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="<?php echo base_url('Site/home');?>">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            
                            
                        </ul>
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> 
                        <!-- <small>blank page layout</small> -->
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->
                   <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                
                                <?php if (isset($message)) { ?>
<CENTER><h5 style="color:green;"><?php echo $message;?></h5></CENTER><br>
<?php } ?>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title" >
                                                <div class="caption" >
                                                    <i class="fa fa-gift"></i>SIGN UP</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="javascript:;" class="expand"> </a>
                                                    <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a> -->
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <!-- <a href="javascript:;" class="remove"> </a> -->
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('Adduser/edituser');?>" class="form-horizontal register-form" method="post">
                                                    <div class="form-body">
                                                         <?php foreach ($results as $key => $values) {
                                                           
                                                              
                                                        ?>
                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label">Full Name</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control " type="text" placeholder="Full Name" name="name" value="<?= $values->name; ?>" /> 
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label">Email</label>
                                                            <div class="col-md-4">
                                                                
                                                                    
                                                                   <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" value="<?= $values->email; ?>"/> 
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                <label class="control-label col-md-3">Number</label>
                                                <div class="col-md-4">
                                                
                                                       
                                                        <input class="form-control placeholder-no-fix" id="mask_phone" type="text" placeholder="Number" name="number" value="<?= $values->number; ?>"/> 
                                                </div>
                                            </div>

                                           
                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label">Section</label>
                                                            <div class="col-md-4">
                                                               
                                                              <select name="lan" class="form-control">
                                                             <option value="<?= $values->lan_id; ?>"><?= $values->section; ?></option>
                                                            <option value="1">PHP</option>
                                                            <option value="2">Android</option>
                                                            <option value="3">Design</option>
                                                        </select>
                                                            </div>
                                                        </div>
                                                        
                                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Role</label>
                                                <div class="col-md-4">
                                                    
                                                       
                                                        <select name="role" class="form-control">
                                                            <option value="<?= $values->roll_id; ?>"><?= $values->roll; ?></option>
                        <option value="">Role of the Employee</option>
                        <option value="1">User</option>
                        <option value="2">Admin</option>
                        <option value="3">Super Admin</option>
                    </select> </div>
                                                
                                            </div>

                                            <div class="form-group">
                                                            <label class="col-md-3 control-label">Username</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" value="<?= $values->username; ?>"/> 
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Password</label>
                                                            <div class="col-md-4">
                                                               
                                                              <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" value="<?= $values->password; ?>" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Retype Your Password</label>
                                                            <div class="col-md-4">
                                                               
                                                               <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword" value="<?= $values->password; ?>"/>
                                                            </div>
                                                        </div>

                                                       <div class="form-group margin-top-20 margin-bottom-20">
                    
                    <div id="register_tnc_error"> </div>
                </div>
                <?php } ?>   
                                           

                                            </div>            
                                             <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <input type="hidden" name="id" value="<?php echo $values->id;?>"/>
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>           
                                                       
                                                        
                                                    
                                                    
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                         </div>
                                        </div>


                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>

                </div>
                <!-- END CONTENT BODY -->
            </div>
            <?php
include('footer.php');
?>