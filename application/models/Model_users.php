
<?php
class Model_users extends CI_Model {
	public function can_login()
	{
		$query=$this->db->query("SELECT * from login ");
		$this->db->where('username',$this->input->post('username'));
		$this->db->where('password',md5($this->input->post('password')));


		$query=$this->db->get('login');

		if($query->num_rows()>0)
		{
			$row = $query->row(); 
			return $row; 
			
		}
		else
		{
			return false;
		}
	}

	public function can_reg($new)
	{
		$this->db->insert("user",$new);
		return true;
	}
	public function view_mem($id)
	{
		$query=$this->db->query("SELECT * from reg ");
		$this->db->where('id',$id);
		$query = $this->db->get('reg');
	 return $query->result();
	}

	public function getid()
	{
		$query=$this->db->query("SELECT id from user ");
		$this->db->where('email',$this->session->userdata('email'));
		$query = $this->db->get('user');
		if($query->num_rows()>0)
		{
			
			$row = $query->row(); 
			return $row->id; 
		}
		else
		{
			return false;
		}	
	}

	public function addlogin($log)
	{
		$this->db->insert("login",$log);
		return true;
	}

	public function addtimesheet($new)
	{
		$this->db->set('Date', 'NOW()', FALSE);
		$this->db->insert("timesheet",$new);
		return true;
	}


	public function addnew($new)
	{
	
		$this->db->insert("timesheet",$new);
		return true;
	}

	public function addproject($new)
	{
		$this->db->insert("project",$new);
		return true;
	}
    
    public function addsalary($data)
	{
		$this->db->insert("salary",$data);
		return true;
	}

	public function addprojectlist($new)
	{
		$this->db->insert("project_list",$new);
		return true;
	}


	public function viewproject()
	{
		$query=$this->db->query("SELECT * from project ");
		
		$query = $this->db->get('project');
	 return $query->result();
	}

	public function viewuser()
	{
		$query=$this->db->query("SELECT * from user");
		
		$query = $this->db->get('user');
	 return $query->result();
	}

	public function viewtimesheet()
	{
		$id=$this->session->userdata('id');
		$this->db->select('timesheet.id as timesheet_id,timesheet.Date,user.name,project.project,timesheet.fromdate,timesheet.todate,timesheet.description,timesheet.approved,login.roll_id');
		$this->db->from('timesheet');
		$this->db->join('user', 'user.id=timesheet.user_id','inner');
		$this->db->join('login', 'login.user_id=user.id','inner');
		$this->db->join('project', 'project.id=timesheet.project_id','inner');
		$this->db->where('user.id',$id);
		$query = $this->db->get();
		return $query->result();

	}

	public function viewprojectlist()
	{
		$query=$this->db->query("SELECT * from project");
		
		$query = $this->db->get('project');
	 return $query->result();
	}



	public function viewsph()
	{
       
		$query=$this->db->query("SELECT * from salary");
		
		$query = $this->db->get('salary');
	    return $query->result();
    }
	public function previlage()
	{
		$id=$this->session->userdata('id');
		$this->db->select('*');
		$this->db->from('login');
		$this->db->join('roll', 'roll.rid=login.roll_id','inner');
		$this->db->join('section', 'section.id=login.lan_id','inner');
		$this->db->where('login.user_id',$id);
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$row = $query->row(); 
			return $row; 
			
		}
		else
		{
			return false;
		}
		
	}

	public function reviewtimesheet()
	{
		$id=$this->session->userdata('id');
		$lan=$this->session->userdata('lan');
		$this->db->select('timesheet.id as timesheet_id,user.name,project.project,timesheet.Date,timesheet.fromdate,timesheet.todate,timesheet.description,timesheet.approved');
		$this->db->from('timesheet');
		$this->db->join('user', 'user.id=timesheet.user_id','inner');
		$this->db->join('project', 'project.id=timesheet.project_id','inner');
		$this->db->join('login', 'user.id=login.user_id','inner');
		$this->db->join('roll', 'roll.rid=login.roll_id','inner');
		$this->db->where('login.roll_id','1');
		$this->db->where('login.lan_id',$lan);
		$query = $this->db->get();
		return $query->result();

	}

	public function editprojectlist()
	{
		$id=$this->session->userdata('id');
		$lan=$this->session->userdata('lan');
		$this->db->select('timesheet.id as timesheet_id,user.name,project.project,timesheet.fromdate,timesheet.todate,timesheet.description,timesheet.approved,timesheet.Date');
		$this->db->from('timesheet');
		$this->db->join('user', 'user.id=timesheet.user_id','inner');
		$this->db->join('project', 'project.id=timesheet.project_id','inner');
		$this->db->join('login', 'user.id=login.user_id','inner');
		$this->db->join('roll', 'roll.rid=login.roll_id','inner');
		$this->db->where('login.roll_id','1');
		$this->db->or_where('login.roll_id','2');
		$this->db->where('login.lan_id',$lan);
		$query = $this->db->get();
		return $query->result();

	}
    //bindya
	public function edit3($sess)
	{
		$query=$this->db->query("SELECT * from project ");
		$this->db->where('id',$sess);
		
		$query = $this->db->get('project');
	 return $query->result();
	}


 

	public function approvetimesheet()
	{
		$id=$this->session->userdata('id');
		$lan=$this->session->userdata('lan');
		$this->db->select('timesheet.id as timesheet_id,user.name,project.project,timesheet.fromdate,timesheet.todate,timesheet.description,timesheet.approved,timesheet.Date');
		$this->db->from('timesheet');
		$this->db->join('user', 'user.id=timesheet.user_id','inner');
		$this->db->join('project', 'project.id=timesheet.project_id','inner');
		$this->db->join('login', 'user.id=login.user_id','inner');
		$this->db->join('roll', 'roll.rid=login.roll_id','inner');
		$this->db->where('login.roll_id','1');
		$this->db->or_where('login.roll_id','2');
		$this->db->where('login.lan_id',$lan);
		$query = $this->db->get();
		return $query->result();

	}


	public function status($sess)
	{
		$data=array('approved'=>0,'approved'=>1);
$this->db->where('id',$sess);
$this->db->update('timesheet',$data);
	}

	public function super_status($sess)
	{
		$data=array('approved'=>1,'approved'=>2);
$this->db->where('id',$sess);
$this->db->update('timesheet',$data);
	}

	public function edit($sess)
	{
		$this->db->select('timesheet.id as timesheet_id,user.name,project.project,project.id,timesheet.fromdate,timesheet.todate,timesheet.description,timesheet.approved');
		$this->db->from('timesheet');
		$this->db->join('user', 'user.id=timesheet.user_id','inner');
		$this->db->join('project', 'project.id=timesheet.project_id','inner');
		$this->db->where('timesheet.id',$sess);
		$query = $this->db->get();
		return $query->result();
		
	}

	public function deletesheet($sess)
	{
		
		$this->db->where('id',$sess);
		$this->db->delete('timesheet');
		
	}

	public function editsheet($new,$i)
	{
		 
		$this->db->where('id',$i);
		$this->db->update('timesheet',$new);
		
	}

    public function editsheet4($new,$i)
	{
		 
		$this->db->where('id',$i);
		$this->db->update('project',$new);
		
	}
	public function log_time($date)
	{
		$this->db->insert("login",$date);
		return true;
	}

	public function log_out($date)
	{
		$this->db->insert("login",$date);
		return true;
	}

	}
	?>