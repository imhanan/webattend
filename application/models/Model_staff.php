<?php
class Model_staff extends CI_Model {
	public function view_user()
	{
		$id=$this->session->userdata('id');
		$lan=$this->session->userdata('lan');
		$this->db->select('user.id,user.name,user.email,user.number,login.username,login.password,login.user_id,roll.roll');
		$this->db->from('user');
	
		
		$this->db->join('login', 'user.id=login.user_id','inner');
		$this->db->join('roll', 'roll.rid=login.roll_id','inner');
		$this->db->where('login.roll_id','1');
		$this->db->or_where('login.roll_id','2');
		
		$query = $this->db->get();
		return $query->result();
	}


	public function edit($sess)
	{
		$this->db->select('user.id,user.name,user.email,user.number,login.username,login.password,login.user_id,roll.roll,login.lan_id,login.roll_id,section.section');
		$this->db->from('user');
		$this->db->join('login', 'user.id=login.user_id','inner');
		$this->db->join('roll', 'roll.rid=login.roll_id','inner');
		$this->db->join('section', 'section.id=login.lan_id','inner');
	
		
		$this->db->where('user.id',$sess);
		$query = $this->db->get();
		return $query->result();
		
	}

	public function edituser($new,$i,$log)
	{
		 
		$this->db->where('id',$i);
		$this->db->update('user',$new);
		$this->db->where('user_id',$i);
		$this->db->update('login',$log);

		
	}

	public function deleteuser($sess)
	{
		
		$this->db->where('id',$sess);
		$this->db->delete('user');
		$this->db->where('user_id',$sess);
		$this->db->delete('login');
		
	}

public function ViewTime()
	{
		/*$sid=$this->session->userdata('sid');*/
		/*$username=$this->session->userdata('time');*/
		// $time = $this->session->userdata('username');
		$this->db->select('user.name,timesheet.fromdate,timesheet.todate,project.project');
		$this->db->from('timesheet');
	
		
		$this->db->join('user', 'user.id=timesheet.user_id','inner');
		$this->db->join('project', 'project.id=timesheet.project_id','inner');
		
		$query = $this->db->get();
		return $query->result();
	}

	public function viewprojectlist($sess)
	{
		
		$this->db->where('id',$sess);
		$this->db->delete('user');
		$this->db->where('user_id',$sess);
		$this->db->delete('login');
		
	}

	public function projectdetail($sess)
	{
		$this->db->select('project.project,project.duration,project.cost,project.language');
		$this->db->from('project');
		$this->db->where('project.id',$sess);
		 
		$query = $this->db->get();
		return $query->result();
		
	}


	public function projectdet($sess)
	{
		$this->db->select('timesheet.user_id,user.name,timesheet.Date,timesheet.fromdate,timesheet.todate,salary.sph');
		$this->db->from('project');
		$this->db->join('timesheet ','project.id=timesheet.project_id','inner');
		$this->db->join('user','user.id=timesheet.user_id','inner');
		$this->db->join('salary','user.id=salary.user_id','inner');
		$this->db->where('project.id',$sess);
		$this->db->group_by('user.name'); 
		$query = $this->db->get();
		// $row = $query->row(); 
		// 	return $row; 
		 return $query->result();
		
}

	public function projectdeta($sess)
	{

		$this->db->select('timesheet.user_id,timesheet.Date,timesheet.fromdate,timesheet.todate,timesheet.description,user.name');
		$this->db->from('timesheet');
		$this->db->join('project','project.id=timesheet.project_id','inner');
		$this->db->join('user','user.id=timesheet.user_id','inner');
		$this->db->where('timesheet.project_id',$sess);
		$query  = $this->db->get();
		return $query->result();
		
	} 


	



}
 

